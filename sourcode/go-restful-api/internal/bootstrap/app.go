package bootstrap

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/cynomous/go-restful-api/internal/controller"
	"gitlab.com/cynomous/go-restful-api/internal/model"
	"gitlab.com/cynomous/go-restful-api/internal/repository"
)

func App() {
	var config = model.Config{
		AppName:    "news_app",
		AppPort:    ":8000",
		TimeZone:   "Asia/Jakarta",
		DBHost:     "localhost",
		DBPort:     "5432",
		DBUsername: "btech",
		DBPassword: "12345",
		DBName:     "news_app",
	}

	// DB CONNECTION ===============================================================================================

	// generate db config
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=%s",
		config.DBHost, config.DBUsername, config.DBPassword, config.DBName, config.DBPort, config.TimeZone,
	)
	// db connection
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err.Error())
	}
	// get native driver sql
	sql, err := db.DB()
	if err != nil {
		log.Fatal(err.Error())
	}
	// check ping
	err = sql.Ping()
	if err != nil {
		log.Fatal(err.Error())
	}

	// MIGRATION DATABASE =========================================================================================
	db.AutoMigrate(model.News{})

	// INIT REPOSITORY =========================================================================================
	newsRepository := repository.NewNewsRepository(db)

	// INIT VALIDATOR =========================================================================================
	validator := validator.New()

	// INIT CONTROLLER =========================================================================================
	newsController := controller.NewNewsController(newsRepository, validator)

	// WEBSERVER ===============================================================================================

	router := gin.Default()
	router.GET("/news", newsController.Get)
	router.POST("/news", newsController.Create)
	router.PUT("/news/:id", newsController.Update)
	router.DELETE("/news/:id", newsController.Delete)
	router.Run(config.AppPort)
}
