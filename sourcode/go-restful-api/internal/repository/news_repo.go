package repository

import (
	"fmt"

	"gitlab.com/cynomous/go-restful-api/internal/model"
	"gorm.io/gorm"
)

type NewsRepository struct {
	db *gorm.DB
}

func NewNewsRepository(db *gorm.DB) *NewsRepository {
	return &NewsRepository{
		db: db,
	}
}

func (repo *NewsRepository) GetAll() ([]model.News, error) {
	var news = []model.News{}
	err := repo.db.Find(&news).Error
	if err != nil {
		return nil, err
	}
	return news, nil
}

func (repo *NewsRepository) GetByID(id uint) (*model.News, error) {
	var news = new(model.News)
	err := repo.db.Where("id = ?", id).Last(&news).Error
	if err != nil {
		return nil, err
	}
	return news, nil
}

func (repo *NewsRepository) Create(data *model.News) (*model.News, error) {
	err := repo.db.Save(data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (repo *NewsRepository) Update(id uint, data *model.News) (*model.News, error) {
	news, err := repo.GetByID(id)
	if err != nil {
		return nil, fmt.Errorf("record not found, cannot update news | error : %s", err.Error())
	}

	news.Title = data.Title
	news.Content = data.Content
	news.Creator = data.Creator
	news.Tag = data.Tag

	err = repo.db.Save(news).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (repo *NewsRepository) Delete(id uint) error {
	err := repo.db.Unscoped().Delete(&model.News{}, "id = ?", id).Error
	if err != nil {
		return fmt.Errorf("cannot delete data | error : %s", err.Error())
	}
	return nil
}
