package response

import (
	"github.com/gin-gonic/gin"
)

func NewsCreateResponse(c *gin.Context, status bool, msg string) {
	if status {
		c.JSON(200, gin.H{
			"status":    status,
			"error_msg": msg,
		})
	} else {
		c.JSON(400, gin.H{
			"status":    status,
			"error_msg": msg,
		})
	}
}

func NewsGetResponse(c *gin.Context, status bool, data interface{}) {
	if status {
		c.JSON(200, gin.H{
			"status": status,
			"data":   data,
		})
	} else {
		c.JSON(400, gin.H{
			"status": status,
			"data":   data,
		})
	}
}

func NewsUpdateResponse(c *gin.Context, status bool, msg string) {
	if status {
		c.JSON(200, gin.H{
			"status":    status,
			"error_msg": msg,
		})
	} else {
		c.JSON(400, gin.H{
			"status":    status,
			"error_msg": msg,
		})
	}
}

func NewsDeleteResponse(c *gin.Context, status bool, msg string) {
	if status {
		c.JSON(200, gin.H{
			"status":    status,
			"error_msg": msg,
		})
	} else {
		c.JSON(400, gin.H{
			"status":    status,
			"error_msg": msg,
		})
	}
}
