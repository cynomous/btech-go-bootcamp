package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	"gitlab.com/cynomous/go-restful-api/internal/model"
	"gitlab.com/cynomous/go-restful-api/internal/pkg/http/response"
	"gitlab.com/cynomous/go-restful-api/internal/repository"
)

type NewsController struct {
	repo      *repository.NewsRepository
	validator *validator.Validate
}

func NewNewsController(repo *repository.NewsRepository, validator *validator.Validate) *NewsController {
	return &NewsController{
		repo:      repo,
		validator: validator,
	}
}

func (controller *NewsController) Get(c *gin.Context) {
	news, err := controller.repo.GetAll()
	if err != nil {
		response.NewsGetResponse(c, false, news)
		return
	}
	response.NewsGetResponse(c, true, news)
}

func (controller *NewsController) Create(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		response.NewsCreateResponse(c, false, err.Error())
		return
	}

	var request = model.NewsRequest{}
	err = json.Unmarshal(body, &request)
	if err != nil {
		response.NewsCreateResponse(c, false, err.Error())
		return
	}

	err = controller.validator.Struct(&request)
	if err != nil {
		response.NewsCreateResponse(c, false, fmt.Sprintf("invalid request parameter | detail : %s", err.Error()))
		return
	}

	_, err = controller.repo.Create(&model.News{
		Title:   request.Title,
		Creator: request.Creator,
		Content: request.Content,
		Tag:     request.Tag,
	})
	if err != nil {
		response.NewsCreateResponse(c, false, fmt.Sprintf("cannot create news | detail : %s", err.Error()))
		return
	}

	response.NewsCreateResponse(c, true, "success create news")
}

func (controller *NewsController) Update(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		response.NewsUpdateResponse(c, false, err.Error())
		return
	}

	var request = model.NewsRequest{}
	err = json.Unmarshal(body, &request)
	if err != nil {
		response.NewsUpdateResponse(c, false, err.Error())
		return
	}

	err = controller.validator.Struct(&request)
	if err != nil {
		response.NewsUpdateResponse(c, false, fmt.Sprintf("invalid request parameter | detail : %s", err.Error()))
		return
	}

	id, _ := strconv.Atoi(c.Param("id"))
	_, err = controller.repo.Update(uint(id), &model.News{
		Title:   request.Title,
		Creator: request.Creator,
		Content: request.Content,
		Tag:     request.Tag,
	})
	if err != nil {
		response.NewsUpdateResponse(c, false, fmt.Sprintf("cannot update news | detail : %s", err.Error()))
		return
	}

	response.NewsUpdateResponse(c, true, "success update news")
}

func (controller *NewsController) Delete(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	err := controller.repo.Delete(uint(id))
	if err != nil {
		response.NewsDeleteResponse(c, false, fmt.Sprintf("cannot delete news | detail : %s", err.Error()))
		return
	}
	response.NewsDeleteResponse(c, true, "success delete news")
}
