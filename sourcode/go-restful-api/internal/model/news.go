package model

import (
	"time"

	"gorm.io/gorm"
)

type News struct {
	gorm.Model
	PublishedAt time.Time
	Title       string `gorm:"type:varchar(255);index"`
	Creator     string `gorm:"type:varchar(150)"`
	Content     string `gorm:"type:text"`
	Tag         string `gorm:"type:text"`
}
