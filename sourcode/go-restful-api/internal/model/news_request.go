package model

type NewsRequest struct {
	Title   string `validate:"required" json:"title"`
	Creator string `validate:"required" json:"creator"`
	Content string `validate:"required" json:"content"`
	Tag     string `validate:"required" json:"tag"`
}
