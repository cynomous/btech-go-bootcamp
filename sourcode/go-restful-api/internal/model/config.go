package model

import "time"

type Config struct {
	AppName      string
	AppPort      string
	TimeZone     string
	TimeLocation *time.Location
	DBHost       string
	DBPort       string
	DBUsername   string
	DBPassword   string
	DBName       string
}
