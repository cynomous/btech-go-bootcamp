package main

import "gitlab.com/cynomous/go-restful-api/internal/bootstrap"

func main() {
	bootstrap.App()
}
