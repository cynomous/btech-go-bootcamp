package main

import (
	"fmt"
	"sync"
	"time"
)

func NewTimer() {
	timer := time.NewTimer(5 * time.Second)
	fmt.Println(time.Now())
	fmt.Println(<-timer.C)
}

func TimeAfter() {
	timer := time.After(3 * time.Second)
	fmt.Println(time.Now())
	fmt.Println(<-timer)
}

func TimeAfterFunc() {
	wg := sync.WaitGroup{}
	wg.Add(1)
	fmt.Println(time.Now())
	time.AfterFunc(time.Second, func() {
		fmt.Println("hellow world")
		wg.Done()
	})
	wg.Wait()
}

func NewTicker() {
	timer := time.NewTicker(time.Second)
	for t := range timer.C {
		fmt.Println(t.String())
	}
}

func NewTick() {
	timer := time.Tick(1 * time.Second)
	for t := range timer {
		fmt.Println(t.String())
	}
}

func main() {
	// NewTimer()
	// TimeAfter()
	// TimeAfterFunc()
	// NewTicker()
	// NewTick()
}
