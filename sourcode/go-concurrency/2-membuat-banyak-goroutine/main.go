package main

import (
	"fmt"
	"runtime"
	"time"
)

func print(message string) {
	fmt.Println(message)
}

func main() {
	runtime.GOMAXPROCS(2)

	for i := 0; i < 1000000; i++ {
		go print("hallo-" + fmt.Sprint(i))
	}

	time.Sleep(10 * time.Second)
}
