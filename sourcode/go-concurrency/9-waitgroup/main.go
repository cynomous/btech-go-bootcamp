package main

import (
	"fmt"
	"runtime"
	"sync"
)

func doPrint(wg *sync.WaitGroup, message string) {
	defer wg.Done()
	fmt.Println(message)
}

func doPrintSync(message string) {
	fmt.Println(message)
}

func main() {
	runtime.GOMAXPROCS(2)

	var wg sync.WaitGroup
	var data = []int{1, 2, 3, 4, 5, 6}
	wg.Add(len(data))

	for v := range data {
		var data = fmt.Sprintf("data %d", v)
		go doPrint(&wg, data)
	}

	wg.Wait()
}
