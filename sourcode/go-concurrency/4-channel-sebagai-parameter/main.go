package main

import (
	"fmt"
	"time"
)

func main() {
	showSkill := func(skill chan string) {
		fmt.Println("waiting skill ...")
		fmt.Println("your skill is : ", <-skill)
		close(skill)
	}

	skill := make(chan string)
	go showSkill(skill)

	time.Sleep(5 * time.Second)
	skill <- "jumpshoot"
	time.Sleep(2 * time.Second)
}
