package main

import (
	"fmt"
	"runtime"
	"time"
)

func print(till int, message string) {
	for i := 0; i < till; i++ {
		fmt.Println((i + 1), message)
	}
}

func main() {
	runtime.GOMAXPROCS(2)

	go print(10, "hallo nanda")
	print(5, "hallo steven")

	time.Sleep(5 * time.Second)
}
