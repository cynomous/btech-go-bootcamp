package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

type counter struct {
	Value int
	sync.Mutex
}

func (c *counter) Incrment() {
	c.Lock()
	c.Value++
	c.Unlock()
}

func main() {
	runtime.GOMAXPROCS(2)

	var ctr = new(counter)

	for i := 0; i < 1000; i++ {
		go func() {
			for j := 0; j < 100; j++ {
				ctr.Incrment()
			}
		}()
	}

	time.Sleep(5 * time.Second)
	fmt.Println(ctr.Value)
}
