package main

import (
	"fmt"
	"time"
)

func ChanIn(ch chan<- string) {
	ch <- "hallo semuanya"
}

func ChanOut(ch <-chan string) {
	fmt.Println("data :", <-ch)
}

func main() {
	ch := make(chan string)

	go ChanIn(ch)
	go ChanOut(ch)

	time.Sleep(5 * time.Second)
	close(ch)
}
